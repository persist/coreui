<?php declare(strict_types=1);

namespace Persist\CoreUIBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CoreUIBundle extends Bundle
{
}
